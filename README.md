# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 


--------------------

# Admin Toolbox

Reusable admin components for the Neoteric Design CMS.

Gear up your ActiveAdmin resources with these commonly needed tools.

## Installation

Neoteric CMS gems are served from a private host. Replace `SECURE_GEMHOST` with the source address.

```ruby
# Gemfile
source SECURE_GEMHOST do
  gem 'admin_toolbox', '~> 0.5.0'
end
```

```sh
$ bundle install
```

## Usage
Call the `admin_toolbox` method and pass in which tools you'd like for that resource.

```
# app/admin/my_model.rb
ActiveAdmin.register MyModel do
  admin_toolbox :previewable, :action_bar, :friendly_findable, :sortable
end
```


## The Toolbox

### ActionBar

Provides a floating container for your form's actions

#### Usage

```scss
// active_admin.scss

@import "admin_toolbox/action_bar";
```

```ruby
# app/admin/my_model.rb

admin_toolbox :action_bar

form do |f|
  # ...
  f.action_bar
end
```

With no actions specified, you get a standard cancel and save actions.

```ruby
f.action_bar
# is equivalent to
f.action_bar :cancel, :save

# Specify the actions you want from the readymade selection
f.action_bar :cancel, :preview, :draft, :publish


# Add your own actions

f.action_bar :cancel, :preview,
             -> { attribute_submit(field: 'state', new_value: 'in_review', label: "Mark for Review") }

# ActionBar adds the `attribute_submit` method to Rails' FormBuilder, to make # setting an attribute with different buttons (like we want for publish/draft)
# easy. Labels can be set through I18n. See config/locales/en.yml for details
```

##### Ready made actions

| Name        | Description                                             |
|-------------|---------------------------------------------------------|
|`:cancel`    | Return to index without save                            |
|`:save`      | Rails default submit action                             |
|`:preview`   | [Live preview](#previewable) of currently entered data  |
|`:draft`     | Save resource with a state of 'drafted'                 |
|`:publish`   | Save resource with a state of 'published'               |

### FriendlyFindable

Configures a resource to use FriendlyId's finder method.

#### Usage

```ruby
# app/admin/my_model.rb

admin_toolbox :friendly_findable
```

### Previewable

Allows resources to be previewed as if they were on the live site, even before saving.

#### Usage

```ruby
# app/admin/my_model.rb

admin_toolbox :previewable
```

Override the `preview_options` method with a hash of your desired settings.

| Option      | Description                                             |
|-------------|---------------------------------------------------------|
|`:controller`| Stringified name of the controller to use to render the template. Default: 'ApplicationControler' |
|`:instance_var` | Name of the instance var to set for the template     |

All other options are passed to the controller's `render` method.

### Sortable

Adds editorial sort to your index table. Defaults to a position column named `position` but can be customized by overriding the `position_field` method in the controller. When constructing your index table, call `sort_column` where you'd like to insert the UI handle for sorting.

#### Usage

Relies on the jQuery sortable table plugin for the dragon drop, it's included with AdminToolbox

```coffeescript
# active_admin.coffee

#= require admin_toolbox/sortable_table

$ ->
  $('table#index_table_plural_model_name').sortable_table()
```

```ruby
# app/admin/my_model.rb

admin_toolbox :sortable

index do
  # ...
  sort_column
  # ...
end
```

