module AdminToolbox
  module Integrations
    module Rails
      def self.inject
        ActionView::Helpers::FormBuilder.include ActionBar
      end

      module ActionBar
        def attribute_submit(field:, new_value:, **options)
          label = options.delete(:label) do
            existing_value = object.send(field).to_s
            I18n.t(".was_#{existing_value}.#{new_value}", scope: [:helpers, :attribute_submit, field],
                                                          default: new_value.to_sym)
          end

          input_name = "#{object.model_name.param_key}[#{field}]"

          default_options = {
            type: :submit,
            name: input_name,
            value: new_value
          }

          button(label, default_options.merge(options))
        end

        def action_bar(*actions, **options)
          actions = %i[cancel save] if actions.blank?
          options[:class] ||= 'actions action-bar'

          template.field_set_tag options.delete(:legend), options do
            actions.each do |action|
              action_content = if action.respond_to?(:call)
                                 action.call
                               elsif default_action_bar_actions.keys.include?(action)
                                 default_action_bar_actions[action].call
                               else
                                 action
                               end

              template.concat(action_content)
            end
            template.concat(yield) if block_given?
          end
        end

        # These defaults are specific to ActiveAdmin's stylesheet but
        # they work well for now.
        def default_action_bar_actions
          {
            cancel: -> { template.content_tag(:li, template.link_to('Cancel', { action: :index }), class: 'cancel') },
            save: -> { submit },
            preview: -> { template.content_tag(:li, template.live_preview_link, class: 'action input_action') },
            draft: lambda {
                     template.content_tag(:li, attribute_submit(field: 'state', new_value: 'drafted'),
                                          class: 'action input_action')
                   },
            publish: lambda {
                       template.content_tag(:li, attribute_submit(field: 'state', new_value: 'published'),
                                            class: 'action input_action')
                     }
          }
        end
      end
    end
  end
end
