module AdminToolbox
  module Integrations
    module ActiveAdmin
      def self.inject
        ::ActiveAdmin::ResourceDSL.include(AdminToolbox::Tools)
        ::ActiveAdmin::Views::IndexAsTable::IndexTableFor.include(SortColumn)
      end

      module SortColumn
        def sort_column(url = nil, heading: :sort, content: nil,
                        html_options: {})
          content ||= "<i class='fa fa-bars'></i>"
          html_options = { class: 'handle' }.merge html_options
          url ||= url_for(controller: active_admin_config.resource_name.plural,
                          action: :sort)
          column heading do
            link_to content.html_safe, url, html_options
          end
        end
      end
    end
  end
end
