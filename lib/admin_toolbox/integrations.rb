Dir[File.dirname(__FILE__) + '/integrations/*.rb'].each { |f| require f }

module AdminToolbox
  module Integrations
    def self.inject!
      ActiveAdmin.inject
      Rails.inject
    end
  end
end
