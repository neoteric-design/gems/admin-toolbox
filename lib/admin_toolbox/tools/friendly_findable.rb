# FriendlyId 5.x+ finder helper

module AdminToolbox
  module Tools
    module FriendlyFindable
      def self.included(base)
        base.controller do
          def find_resource
            scoped_collection.friendly.find(params[:id])
          end
        end
      end
    end
  end
end