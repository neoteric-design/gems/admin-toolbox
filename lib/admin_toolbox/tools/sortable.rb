module AdminToolbox
  module Tools
    module Sortable
      def self.included(base)
        base.send(:collection_action, :sort, :method => :post) do
          params[param_name].each_with_index do |id, index|
            resource_class.unscoped.where(id: id).update_all(position_field => index + 1)
          end

          head :ok
        end

        base.controller do
          def position_field
            :position
          end

          def param_name
            resource_class.model_name.singular.to_sym
          end
        end

        base.send(:config).sort_order = "#{base.controller.new.position_field}_asc"
      end
    end
  end
end