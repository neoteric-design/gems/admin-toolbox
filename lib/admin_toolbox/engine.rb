module AdminToolbox
  class Engine < ::Rails::Engine
    initializer 'admin_toolbox.integrations',
                :after => :load_config_initializers do

      Integrations.inject!
    end
  end
end
