Dir[File.dirname(__FILE__) + '/tools/*.rb'].each { |f| require f }

module AdminToolbox
  module Tools
    def admin_toolbox(*tools)
      tools.each do |tool|
        include "AdminToolbox::Tools::#{tool.to_s.camelize}".constantize
      end
    end
  end
end
