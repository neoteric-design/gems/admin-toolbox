# AdminToolbox Changelog

0.5.8
=====

* Ruby 3 fixes

0.5.7
=====

* Fix preview JS failing on some browsers

0.5.6
=====

* Replace deprecated `render nothing: true` with `head :ok` on sortable. Fixes
  Harmless but noisy error

0.5.5
=====

* Update live preview JS: fix breakage when CK4 not present,
  replace jquery functions with vanilla js

0.5.4
=====

* Fix sortable's sort_order configuration for ActiveAdmin. AA is now stricter about the format it recogizes.

0.5.3
=====

* Improve live preview javascript. No more document.write so the front end view
  should render cleanly. Also remove window.open call to maybe avoid popup
  blockers

0.5.2
=====

* Fix/Improvement: attribute_submit translation lookup now handles nil existing
  values. As well allows for a default value per field.

0.5.1
=====

* Fixed: Preview now forwards the admin request enviroment. Fixes Devise/Warden
  expected middleware data not being present

0.5.0
=====

* Enhanced: Rework previewable to use any given Controller for rendering.
  This gives greater flexibility in how the preview is rendered, as well as fixing the long time issue of rendering partials from a different controller namespace.
* Fixed: ActionBar default actions now properly overwritten
* Fixed: ActionBar CSS margin tweaks

0.4.0
=====

* ActionBar!
* Misc refactorings
