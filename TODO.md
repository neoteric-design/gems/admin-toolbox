# AdminToolbox TODO

## ActionBar

* Maybe include some Formtastic hooks for things like the default actions so they're cleaner and less hyper-specific to ActiveAdmin's FormBuilder

* Attribute Submit and ActionBar are married and that may or may not be the best way to implement but it's not hurting anything for now.
