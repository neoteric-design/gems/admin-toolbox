var plugin = 'sortable_table',
SortableTable = function(el, options) {
  this.$el = $(el);
  this.options = $.extend({
    handle: ".handle",
    axis: "y",
    helper: function(e, ui) {
      ui.children().each(function() {
        $(this).width($(this).width());
      });
      return ui;
    },
    update: function(e, ui) {
      var serialized = $(this).sortable('serialize'),
          url = $(this).find('.handle').first().attr('href');
      $.post(url, serialized, function(data) {});
    }
  }, options);

  this.$el.find('tbody').sortable(this.options)
    .disableSelection()
    .on('click', '.handle', function(e) {
      e.preventDefault();
    });
};

$.fn[plugin] = function(options) {
  this.each(function() {
    if (!$.data(this, plugin)) {
      return $.data(this, plugin, new SortableTable(this, options));
    }
  });
};
